response = HTTParty.get('https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json')
response = response.parsed_response
eval(response).map{ |e| e[:name] }


# Alternative solution

response = HTTParty.get('https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json')
response = response.parsed_response
response = response.gsub("'", '"').gsub('name:', '"name":').gsub('code:', '"code":').gsub(' \n  ', '')
JSON.parse(response).map{ |e| e['name'] }
